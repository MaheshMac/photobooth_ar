// Fill out your copyright notice in the Description page of Project Settings.

#include "MyARPawn.h"


// Sets default values
AMyARPawn::AMyARPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMyARPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyARPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	MovePlayerCard();
}

// Called to bind functionality to input
void AMyARPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AMyARPawn::MovePlayerCard()
{
	float XLoc001;
	float YLoc001;
	bool bisPressed001;
	float XLoc002;
	float YLoc002;
	bool bisPressed002;
	GetWorld()->GetFirstPlayerController()->GetInputTouchState(ETouchIndex::Touch1, XLoc001, YLoc001, bisPressed001);
	GetWorld()->GetFirstPlayerController()->GetInputTouchState(ETouchIndex::Touch2, XLoc002, YLoc002, bisPressed002);
	FVector2D TouchOneLoc(XLoc001, YLoc001);


	if (bisPressed001 && bisPressed002 && binMoveMode)
	{
		FVector DeproWorldLocation;
		FVector DeproWorldDirection;
		GetWorld()->GetFirstPlayerController()->DeprojectScreenPositionToWorld(XLoc001, YLoc001, DeproWorldLocation, DeproWorldDirection);

		if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Some variable values: x: %f, y: %f, z: %f"), DeproWorldDirection.X, DeproWorldDirection.Y, DeproWorldDirection.Z));
		}

		if (XisSelected)
		{
			FVector MoveOffset;
			MoveOffset.X = DeproWorldDirection.X;
			SelectedComponent->AddLocalOffset(MoveOffset);
		}
		else 
		{
			FVector MoveOffset;
			MoveOffset.Y = DeproWorldDirection.Y;
			SelectedComponent->AddLocalOffset(MoveOffset);
		}
	}
}

void AMyARPawn::ScreenShot()
{
	UE_LOG(LogTemp, Warning, TEXT("ScreenShot Taken"));
}

